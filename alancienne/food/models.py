from django.db import models


class Product(models.Model):

    product_name = models.CharField(max_length=200)
    price_excluding_tax = models.DecimalField(max_digits=5, decimal_places=2)
    stock_order = models.PositiveIntegerField()
    maximum_stock = models.PositiveIntegerField()
    tva = models.FloatField(choices=[(5.50, 5.50), (20.0, 20.0)])

    def price_ttc(self):
        return round(float(self.price_excluding_tax) + (float(self.price_excluding_tax) * (self.tva/100)), 2)

    def stock_disponible(self):
        return self.maximum_stock - self.stock_order


class Cart(models.Model):
    checked_out = models.BooleanField(default=False)

    def total_ttc(self):
        All_items = Item.objects.filter(cart=self)
        total_price = 0
        for item in All_items:
            total_price += item.quantity * item.product.price_ttc()
        return round(total_price, 2)


class Item(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
