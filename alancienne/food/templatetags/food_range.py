from django import template

register = template.Library()


@register.filter
def food_range(quantity):
    return list(range(int(quantity) + 1))
