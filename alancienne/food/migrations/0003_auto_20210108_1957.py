# Generated by Django 3.1.5 on 2021-01-08 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0002_auto_20210105_2050'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='cart_id',
            new_name='cart',
        ),
        migrations.RenameField(
            model_name='item',
            old_name='product_id',
            new_name='product',
        ),
        migrations.AlterField(
            model_name='product',
            name='tva',
            field=models.FloatField(choices=[('5.5', 'Tva Min'), ('20.0', 'Tva Max')]),
        ),
    ]
