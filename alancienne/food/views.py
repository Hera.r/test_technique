from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import *
from .forms import *
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import json


def inventory_admin(request):
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, form['product_name'].value(
            ) + " succesfully add to your inventory")
            return redirect('inventory_admin')
        else:
            print('error')

    context = {'inventory_form': form}

    return render(request, 'food/inventory_admin.html', context)


def catalog(request):
    products = Product.objects.all()
    prices_ttc = []
    quantity_max = []
    for p in products:
        prices_ttc.append(p.price_ttc())
        quantity_max.append(p.stock_disponible())
    product_and_price = zip(products, prices_ttc, quantity_max)

    last_cart = Cart.objects.all().last()
    if last_cart is None or last_cart.checked_out == True:
        last_cart = Cart.objects.create()

    context = {'products': products,
               'product_and_price': product_and_price}

    return render(request, 'food/catalog.html', context)


@csrf_exempt
def update_catalog(request):
    if request.is_ajax() and request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)

        quantity = body['quantity']
        product_name = body['product_name']
        quantities = body["quantities"]

        last_cart = Cart.objects.all().last()
        product = Product.objects.get(product_name=product_name)

        obj, created = Item.objects.update_or_create(
            cart=last_cart, product=product, defaults={'quantity': 0})
        obj.quantity = quantity
        obj.save()
        # mise à jour des quantités
        all_items = Item.objects.filter(cart=last_cart).order_by("product_id")
        for i in range(len(all_items)):
            all_items[i].quantity = quantities[i]
            all_items[i].save()

        prix_total = last_cart.total_ttc()

        context = {'quantity': quantity,
                   'prix_total': prix_total,
                   }

        body["prix_total"] = prix_total
        body["total_quantity"] = sum(quantities)
        return JsonResponse(body)
    return render(request, 'food/catalog.html', context)
