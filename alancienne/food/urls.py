from django.urls import path
from . import views

urlpatterns = [
    path('inventory_admin', views.inventory_admin, name='inventory_admin'),
    path('update_catalog', views.update_catalog),
    path('catalog', views.catalog, name='catalog')

]
