$('.one-select').change(function () {
    var quantity_selected = $(this, "option: selected = selected").val();
    var name_product = $(this).parent().parent().children(".name").text();
    var all_quantity = $('.one-select');
    var quantities = [];
    for (var i = 0; i < all_quantity.length; i++) {
        quantities.push(all_quantity[i].options['selectedIndex']);
    };


    var dataaa = {
        "quantity": quantity_selected,
        "product_name": name_product,
        'prix_total': $("#prix_total").val(),
        'quantities': quantities,
        'total_quantity': 0,
    }
    console.log(dataaa)
    $.ajax({
        type: "POST",
        url: "/food/update_catalog",
        data: JSON.stringify(dataaa),
        contentType: 'application/json',
        success: function (response) {
            $("#prix_total").text(response.prix_total);
            $("#total_quantity").text(response.total_quantity);



        }.bind(this),
        error: function (xhr, status, err) {
            console.error('erreur');
        }.bind(this)
    });


});