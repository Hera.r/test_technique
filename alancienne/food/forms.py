from django.forms import ModelForm
from food.models import Product


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

    def clean_product_name(self):
        return self.cleaned_data['product_name'].lower()
